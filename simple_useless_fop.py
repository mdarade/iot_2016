#!/usr/bin/env python

import os
import sys
from time import sleep
from datetime import datetime

pid = str(os.getpid())
pidfile = "/home/md/tmp.pid"

if os.path.isfile(pidfile):
    print "%s already exists, exiting" % pidfile
    sys.exit()
file(pidfile, 'w').write(pid)
try:
	# Do some actual work here
	fn = '/root/msgs.txt'                                                                 
	f = open(fn, 'aw')
	f.write('%s\n' % datetime.now())
	f.close()
finally:
	os.unlink(pidfile)

